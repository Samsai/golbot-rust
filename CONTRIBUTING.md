# Contributing

When contributing to this repository, please communicate your intention and
motivation to make a change before writing the change itself. This will
reduce potential work duplication and spare you from unnecessary work if the
change would be unlikely to be mainlined. Discussion can take place via
email or via the issue tracker.

## Pull request process

The project prefers changes to be made via the use of Gitlab's pull request
system. Contributions made in other ways may be accepted but are not
encouraged, as PRs allow for easier tracking and merging of changes.

For your pull request you should:

1. Open an issue, describing the feature or bug fix your changes are intended to implement.
2. Make the changes to the source code within the scope of your opened issue. Prefer multiple issues and PRs over single colossal PRs.
3. Ensure your changes work. You should ensure the PR compiles and your intended feature works. Previous unit tests must complete. New unit tests are encouraged but not strictly required.
4. Open a pull request to the project repository. Attach the PR to your original issue number and label it appropriately. Mark if the PR is work-in-progress or finished.

## Code of conduct

At the moment of writing, this project doesn't have enough community or communication
surrounding it that would make a code of conduct necessary.

However, you should still remain polite when dealing with issues, PRs or other
communications with the project maintainers or other users/contributors. Rude or
otherwise unproductive behaviour will be addressed as necessary and if necessary,
a proper code of conduct will be adopted to ensure reasonable quality of discourse.