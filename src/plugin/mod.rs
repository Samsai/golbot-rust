use std::sync::Arc;
use std::sync::mpsc::*;

use super::*;

pub mod pingpong;
pub mod rss_plugin;
// pub mod hello;
pub mod seen;
pub mod tell;
pub mod url_reader;
pub mod notice;

pub trait Plugin {
    fn init(&mut self, sender: Sender<String>);
    fn name(&self) -> String;
    fn process_line(&mut self, line: &String);
}
