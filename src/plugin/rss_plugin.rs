extern crate rss;

extern crate ureq;

use super::*;

use chrono::prelude::Utc;
use chrono::Duration as ChronoDuration;

use std::io::BufReader;

pub struct RssReader {
    config: Arc<BotConfig>,
    rss_feed: String,
    rss_delay: i64,
}

impl RssReader {
    pub fn new(
        config: Arc<BotConfig>,
        pl_config: Arc<Value>,
    ) -> RssReader {
        let feed = pl_config.get("rss");
        let mut rss_feed: String = String::from("http://lorem-rss.herokuapp.com/feed");
        let mut rss_delay: i64 = 0;

        if let Some(val) = feed {
            if val.is_str() {
                rss_feed = String::from(val.as_str().unwrap());
            }
        }

        let delay = pl_config.get("rss_delay");

        if let Some(val) = delay {
            if val.is_integer() {
                rss_delay = val.as_integer().unwrap();
            }
        }

        return RssReader {
            config: config,
            rss_feed,
            rss_delay,
        };
    }
}

impl Plugin for RssReader {
    fn init(&mut self, sender: Sender<String>) {
        let feed = self.rss_feed.clone();
        let config = self.config.clone();
        let delay = self.rss_delay;

        thread::spawn(move || {
            let mut last_posted: i64 = 0;
            let mut discarded = false;

            println!("Started RSS plugin");

            'rss: loop {
                let resp = ureq::get(feed.as_str()).call();

                let rss_channel_result = rss::Channel::read_from(BufReader::new(resp.into_reader()));

                if rss_channel_result.is_ok() {
                    let current_time = (Utc::now() - ChronoDuration::minutes(delay)).timestamp();

                    let mut rss_channel = rss_channel_result.unwrap();

                    let items = rss_channel.items_mut();
                    items.reverse();

                    for item in items.iter() {
                        let date = item.pub_date();

                        match date {
                            None => (),
                            Some(s) => {
                                let date_time = chrono::DateTime::parse_from_rfc2822(s).unwrap();

                                if date_time.timestamp() > last_posted
                                    && date_time.timestamp() < current_time
                                {
                                    last_posted = date_time.timestamp();

                                    if discarded {
                                        sender.send(form_chan_msg(
                                            &config,
                                            format!(
                                                "RSS: {} {}",
                                                item.title().unwrap(),
                                                item.link().unwrap()
                                            ),
                                        )).expect("RSS: Communication channel broken.");

                                        thread::sleep(Duration::from_secs(5));
                                    }
                                }
                            }
                        }
                    }

                    if !discarded {
                        discarded = true;
                    }
                }
                thread::sleep(Duration::from_secs(10));
            }
        });
    }

    fn name(&self) -> String {
        return String::from("RSS");
    }

    fn process_line(&mut self, _line: &String) {

    }
}
