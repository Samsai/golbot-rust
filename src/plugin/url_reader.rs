extern crate htmlescape;
extern crate ureq;

use super::*;

use htmlescape::decode_html;

pub trait HtmlReader {
    fn read_url(&self, url: &str) -> Option<String>;
}

struct UReqReader {
    user_agent: String,
}

impl UReqReader {
    fn new(user_agent: String) -> UReqReader {
        return UReqReader {
            user_agent,
        };
    }
}

impl HtmlReader for UReqReader {
    fn read_url(&self, url: &str) -> Option<String> {
        println!("Reading url {}", url);

        let resp = ureq::get(&url)
            .set("Accept-Language", "en-us")
            .set("User-Agent", self.user_agent.as_str())
            .call();

        println!("{}", resp.status_line());
        if !resp.ok() {
            return None;
        }

        let reader = resp.into_reader();
        let mut bytes = Vec::new();
        let mut handle = reader.take(10000);

        if let Ok(_) = handle.read_to_end(&mut bytes) {
            let body = String::from_utf8_lossy(&bytes);

            return Some(body.into_owned());
        } else {
            return None;
        }
    }
}

pub struct UrlReader {
    config: Arc<BotConfig>,
    ignore: Vec<String>,
    youtube_butt: bool,
    reader: Box<dyn HtmlReader>,
    sender: Option<Sender<String>>,
}

impl UrlReader {
    pub fn new(config: Arc<BotConfig>, plugin_config: Arc<Value>) -> UrlReader {
        let user_agent = plugin_config.get("user_agent");

        let user_agent_string = match user_agent {
            Some(val) => {
                if val.is_str() {
                    println!("Using User-Agent: {}", val.as_str().unwrap());
                    String::from(val.as_str().unwrap())
                } else {
                    String::from("")
                }
            }
            None => String::from(""),
        };

        let youtube_butt = plugin_config.get("youtube_butt");

        let youtube_butt_val = match youtube_butt {
            Some(val) => {
                if val.is_bool() {
                    val.as_bool().unwrap()
                } else {
                    false
                }
            }
            None => false,
        };

        if youtube_butt_val {
            println!("YouTube Buttery enabled.")
        }

        return UrlReader::new_with_reader(
            config,
            youtube_butt_val,
            plugin_config,
            Box::new(UReqReader::new(user_agent_string)),
        );
    }

    pub fn new_with_reader(
        config: Arc<BotConfig>,
        youtube_butt: bool,
        plugin_config: Arc<Value>,
        reader: Box<dyn HtmlReader>,
    ) -> UrlReader {
        let mut ignore_list: Vec<String> = Vec::new();

        let value = plugin_config.get("url_ignore");

        match value {
            Some(val) => {
                if val.is_array() {
                    let list = val.as_array().unwrap();

                    for item in list.iter() {
                        println!("Added a new guy to ignore.");
                        ignore_list.push(String::from(item.as_str().unwrap()))
                    }
                }
            }
            None => (),
        }

        return UrlReader {
            config: config,
            ignore: ignore_list,
            youtube_butt: youtube_butt,
            reader: reader,
            sender: None,
        };
    }

    fn clean_url(&self, url: &str) -> String {
        let mut cleaned_url = url.to_string();
        if url.starts_with('<') && url.ends_with('>') {
            cleaned_url = cleaned_url[1..url.len() - 1].to_string();
        }

        if url.contains("youtube.com") && self.youtube_butt {
            cleaned_url = cleaned_url.replace("www.youtube.com", "invidious.snopyta.org");
            cleaned_url = cleaned_url.replace("youtube.com", "invidious.snopyta.org");
        }

        return cleaned_url;
    }
}

impl Plugin for UrlReader {
    fn init(&mut self, sender: Sender<String>) {
        self.sender = Some(sender);
    }

    fn name(&self) -> String {
        return String::from("UrlReader");
    }

    fn process_line(&mut self, line: &String) {
        let line_clone = line.clone();
        let msg = parse_chan_msg(&line_clone);

        if let Some(sender) = &self.sender {
            if let Some((nick, _chan, msg)) = msg {
                let mut ignore = false;

                for name in &self.ignore {
                    if &nick == name {
                        println!("Ignore this guy.");
                        ignore = true;
                        break;
                    }
                }

                if (msg.contains("http://") || msg.contains("https://")) && !ignore {
                    let split: Vec<&str> = msg.split_whitespace().collect();

                    for word in split {
                        //println!("Word: {}", word);

                        if word.contains("http") {
                            let url = self.clean_url(word);
                            let body = self.reader.read_url(&url);

                            lazy_static! {
                                static ref HTML_TITLE_REGEX: Regex =
                                    Regex::new(r"<title>(\n)*(.*?)(\n)*</title>").unwrap();
                            }

                            body
                                .map(|body_text| {
                                    println!("Body exists.");

                                    let capture = HTML_TITLE_REGEX
                                        .captures(&body_text)
                                        .map(|cap| cap.get(2))
                                        .flatten();

                                    let title = capture
                                        .map(|m| m.as_str().trim());

                                    let decoded_title = title.map(|t| decode_html(t));

                                    if let Some(title) = title {
                                        if let Ok(title) = decoded_title.unwrap() {
                                            sender.send(form_chan_msg(&self.config,
                                                    format!("URL: {}", title))).unwrap();
                                        } else {
                                            sender.send(form_chan_msg(&self.config,
                                                    format!("URL: {}", title))).unwrap();
                                        }
                                    }
                                });
                        }
                    }
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    struct StubReader {
        body: String,
    }

    impl HtmlReader for StubReader {
        fn read_url(&self, _url: &str) -> Option<String> {
            return Some(self.body.clone());
        }
    }

    #[test]
    fn can_find_title() {
        let stub = StubReader { body: String::from("<head><title>Hello World</title></head>") };

        let mut url_reader = UrlReader::new_with_reader(
            Arc::new(BotConfig::debug_defaults()),
            true,
            Arc::new(Value::Boolean(true)),
            Box::new(stub),
        );

        let (sender, receiver) = channel();

        url_reader.init(sender);

        let msg = form_chan_msg(
            &BotConfig::debug_defaults(),
            String::from(":testing!test@testing PRIVMSG #golbottest :http://www.testing.com"),
        );

        url_reader.process_line(&msg);

        assert!(receiver.try_recv().is_ok());
    }

    #[test]
    fn can_find_title_surrounded_url() {
        let stub = StubReader { body: String::from("<head><title>Hello World</title></head>") };

        let mut url_reader = UrlReader::new_with_reader(
            Arc::new(BotConfig::debug_defaults()),
            true,
            Arc::new(Value::Boolean(true)),
            Box::new(stub),
        );

        let (sender, receiver) = channel();

        url_reader.init(sender);

        let msg = form_chan_msg(
            &BotConfig::debug_defaults(),
            String::from(":testing!test@testing PRIVMSG #golbottest :<http://www.testing.com>"),
        );

        url_reader.process_line(&msg);

        assert!(receiver.try_recv().is_ok());
    }

    #[test]
    fn will_only_read_first_title() {
        let stub = StubReader { body: String::from("<head><title>Hello World</title></head><title>Wrong</title>") };

        let mut url_reader = UrlReader::new_with_reader(
            Arc::new(BotConfig::debug_defaults()),
            true,
            Arc::new(Value::Boolean(true)),
            Box::new(stub),
        );

        let (sender, receiver) = channel::<String>();
        url_reader.init(sender);

        let msg = form_chan_msg(
            &BotConfig::debug_defaults(),
            String::from(":testing!test@testing PRIVMSG #golbottest :http://www.testing.com"),
        );

        url_reader.process_line(&msg);

        assert!(!receiver.recv_timeout(Duration::from_secs(1)).unwrap().contains("Wrong"));
    }

    #[test]
    fn clean_url_returns_correct_url() {
        let stub = StubReader { body: String::from("<head><title>Hello World</title></head><title>Wrong</title>") };

        let mut url_reader = UrlReader::new_with_reader(
            Arc::new(BotConfig::debug_defaults()),
            true,
            Arc::new(Value::Boolean(true)),
            Box::new(stub),
        );

        let unclean_url = "<http://example.com>";

        assert_eq!(url_reader.clean_url(unclean_url), String::from("http://example.com"));
    }

    #[test]
    fn clean_url_replaces_youtube_with_invidious() {
        let stub = StubReader { body: String::from("<head><title>Hello World</title></head><title>Wrong</title>") };

        let mut url_reader = UrlReader::new_with_reader(
            Arc::new(BotConfig::debug_defaults()),
            true,
            Arc::new(Value::Boolean(true)),
            Box::new(stub),
        );

        let unclean_url = "<http://www.youtube.com>";

        assert_eq!(url_reader.clean_url(unclean_url), String::from("http://invidious.snopyta.org"));
    }

    #[test]
    fn clean_url_replaces_youtube_short_with_invidious() {
        let stub = StubReader { body: String::from("<head><title>Hello World</title></head><title>Wrong</title>") };

        let mut url_reader = UrlReader::new_with_reader(
            Arc::new(BotConfig::debug_defaults()),
            true,
            Arc::new(Value::Boolean(true)),
            Box::new(stub),
        );

        let unclean_url = "<http://youtube.com>";

        assert_eq!(url_reader.clean_url(unclean_url), String::from("http://invidious.snopyta.org"));
    }

    #[test]
    fn actual_title() {
        let reader = UReqReader::new(String::new());

        let mut url_reader = UrlReader::new_with_reader(
            Arc::new(BotConfig::debug_defaults()),
            true,
            Arc::new(Value::Boolean(true)),
            Box::new(reader),
        );

        let (sender, receiver) = channel();

        url_reader.init(sender);

        let msg = form_chan_msg(
            &BotConfig::debug_defaults(),
            String::from(":testing!test@testing PRIVMSG #golbottest :http://www.gamingonlinux.com"),
        );

        url_reader.process_line(&msg);

        assert!(receiver.recv().is_ok());
    }
}
