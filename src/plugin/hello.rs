use super::*;

pub struct Hello {
    config: Arc<BotConfig>,
}

impl Hello {
    pub fn new(config: Arc<BotConfig>) -> Hello {
        return Hello {config: config};    
    }
}

impl Plugin for Hello {
    fn name(&self) -> String {
        return String::from("Hello");
    }

    fn process_line(&mut self, line: &String) -> Option<String> {
        let line_clone = line.clone();
        let msg = parse_chan_msg(&line_clone);

        match msg {
            Some((nick, chan, msg)) => {
                if msg.contains(format!("hello, {}", self.config.nick).as_str()) {
                    return Some(form_chan_msg(&self.config, format!("Hello, {}!", &nick)))
                }
            },
            None => return None,
        }

        return None;
    }
}
