extern crate chrono;

use super::*;

use self::chrono::prelude::*;
use std::collections::HashMap;

pub struct LastSeen {
    config: Arc<BotConfig>,
    seen_db: HashMap<String, (String, DateTime<Utc>)>,
    sender: Option<Sender<String>>
}

impl LastSeen {
    pub fn new(config: Arc<BotConfig>) -> LastSeen {
        return LastSeen {
            config: config,
            seen_db: HashMap::new(),
            sender: None,
        };
    }
}

impl Plugin for LastSeen {
    fn init(&mut self, sender: Sender<String>) {
        self.sender = Some(sender);
    }

    fn name(&self) -> String {
        return String::from("LastSeen");
    }

    fn process_line(&mut self, line: &String) {
        let line_clone = line.clone();
        let msg = parse_chan_msg(&line_clone);

        if let Some(sender) = &self.sender {
            if let Some((nick, _chan, msg)) = msg {
                if msg.starts_with("seen ") {
                    let split: Vec<&str> = msg.split_whitespace().collect();

                    if split.len() > 1 {
                        let nick = String::from(split[1]);

                        if let Some((msg, date_time)) = self.seen_db.get(&nick) {
                            sender.send(form_chan_msg(
                                &self.config,
                                format!(
                                    "I last saw {} at {} saying: {}",
                                    &nick,
                                    &date_time.format("%Y-%m-%d %H:%M:%S UTC").to_string(),
                                    &msg
                                ),
                            )).unwrap();
                        }

                    }
                } else {
                    self.seen_db.insert(nick, (msg, Utc::now()));
                }
            }
        }
    }
}
