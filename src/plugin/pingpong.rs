use super::*;

pub struct PingPong {
    sender: Option<Sender<String>>,
}

impl PingPong {
    pub fn new() -> PingPong {
        return PingPong {
            sender: None
        };
    }
}

impl Plugin for PingPong {
    fn init(&mut self, sender: Sender<String>) {
        self.sender = Some(sender);
    }

    fn name(&self) -> String {
        return String::from("PingPong");
    }

    fn process_line(&mut self, line: &String) {
        if let Some(sender) = &self.sender {
            let line_clone = line.clone();
            let split_line: Vec<&str> = line_clone.split_whitespace().collect();

            if split_line.len() > 1 && split_line[0] == "PING" {
                sender.send(format!("PONG {}\r\n", split_line[1])).unwrap();
            }
        }
    }
}
