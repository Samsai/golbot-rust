FROM debian:latest

RUN apt-get update && apt-get install -y curl
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs > rustup.sh
RUN sh rustup.sh -y
RUN apt-get install -y build-essential
COPY . ./golbot-rust
WORKDIR ./golbot-rust
run $HOME/.cargo/bin/cargo build --release

CMD $HOME/.cargo/bin/cargo run --release
