# GOLBot-Rust, GamingOnLinux IRC Bot Rust rewrite

[![pipeline status](https://gitlab.com/Samsai/golbot-rust/badges/master/pipeline.svg)](https://gitlab.com/Samsai/golbot-rust/commits/master) [![codecov](https://codecov.io/gl/Samsai/golbot-rust/branch/master/graph/badge.svg)](https://codecov.io/gl/Samsai/golbot-rust)

## What is it?

GOLBot-Rust is a rewrite of the older [GOLBotV2](https://gitlab.com/Samsai/GOLBotV2) IRC bot,
used on the gamingonlinux.com IRC channel. This marks the third major iteration of the
GOLBot IRC bot.

GOLBot's tasks include posting links from an RSS feed to reading URLs for web page titles and
allowing users to check when someone has been last seen and leaving each other messages.

## Why a third iteration?

The previous version of GOLBot was hacked together by two people with little regard to
maintainability. As such, the versions we ended up with were more akin to spaghetti than
readable program code. This made making changes and improvements to the bot increasingly
difficult.

GOLBot-Rust is a total rewrite of the codebase and aims to separate each part of the bot
properly, making changes and additions significantly easier.

## Usage

GOLBot-Rust uses two configuration files: **botconfig.toml** and **plugins.toml**.

### botconfig.toml

The general structure of the **botconfig.toml** is as follows:

```toml
    nick = "GOLBOTv3"
    serv = "chat.freenode.net"
    channel = "#golbottest"
    realname = "I have none"
    ident = "GOLBOT"
    password = "somepassword" # optional
```

An [example file](https://gitlab.com/Samsai/golbot-rust/blob/master/botconfig-example.toml) is provided in the repository.

In the **botconfig.toml** file you can define the nickname, server and channel, 
along with other identifying information used to connect the bot to the desired IRC server.

### plugins.toml

The **plugins.toml** file is used to store any plugin specific, largely optional,
details. You can see [an example file here](https://gitlab.com/Samsai/golbot-rust/blob/master/plugins-example.toml), which defines an RSS feed for the RSS plugin
to read and a nickname to ignore in the URL Reader plugin.

## Extending the bot

The bot can be extended by writing plugins for it. I use the term "plugin" fairly loosely,
however, since plugins cannot be loaded during runtime. Instead the plugins are compiled
into the bot upon building. The plugin system is more of an abstraction layer that
separates the internals of the IRC bot from the code that is run on top of the bot.

I recommend you read the sources at [src/plugins/](https://gitlab.com/Samsai/golbot-rust/tree/master/src/plugin)
to have an idea how a GOLBot plugin behaves. Better documentation about plugins will be
written when I bother writing some.
